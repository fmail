#!/usr/bin/env python

import smtplib
import time
import threading

SMTP_HOST = 'localhost'
SMTP_PORT = 12000
MSG_SIZE = 256
MSG_COUNT = 100		#Messages to Send per Test
CONN_COUNT = 20		#Messages to send on Connection test, beware it could be slow
THREAD_COUNT = [2, 5, 10] #, 15, 20, 25, 30, 50, 75, 100]

MSG_FROM = 'testmail@mymail.com'
MSG_TO = 'testuser@localhost'
MSG_DATA = str().zfill(MSG_SIZE)

def SingleSession():
	print 'Benchmarking Single Session'
	
	#Initialize Measuring Variables
	total_time = 0.0
	
	#Connect to Server
	server = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
	
	#Send Messages
	for i in xrange(MSG_COUNT):
		time_start = time.time()
		server.sendmail(MSG_FROM, MSG_TO, MSG_DATA)
		time_end = time.time()
		total_time += time_end - time_start
	
	#Disconnect from Server
	server.quit()
	
	#Calculate Avg time per msg and Messages Per Second
	msg_avg = total_time / MSG_COUNT
	mps = 1.0 / msg_avg
	
	print ' * Total time:', total_time
	print ' * Avg. Time per message:', msg_avg
	print ' * Messages Per second:', mps
	print ' * Total Message Data Transmitted:', MSG_SIZE * MSG_COUNT, 'bytes'
	print ' * Data Rate:', (MSG_SIZE * MSG_COUNT) / total_time, 'bytes/second'

def ConnectionBench():
	print 'Benchmarking Connection Speed'
	print ' - Connection Count:', CONN_COUNT
	#Initialize Measuring Variables
	total_time = 0.0
	
	#Send Messages
	for i in xrange(CONN_COUNT):
		time_start = time.time()
		#Connect to Server
		server = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
		server.sendmail(MSG_FROM, MSG_TO, MSG_DATA)
		server.quit()
		time_end = time.time()
		total_time += time_end - time_start
	
	#Calculate Avg time per msg and Messages Per Second
	msg_avg = total_time / CONN_COUNT
	mps = 1.0 / msg_avg
	
	print ' * Total time:', total_time
	print ' * Avg. Time per message:', msg_avg
	print ' * Messages Per second:', mps
	print ' * Total Message Data Transmitted:', MSG_SIZE * CONN_COUNT, 'bytes'
	print ' * Data Rate:', (MSG_SIZE * CONN_COUNT) / total_time, 'bytes/second'

class ThreadBench(threading.Thread):
	def __init__(self, msgcount):
		threading.Thread.__init__(self)
		self.msgcount = msgcount
	def run(self):
		conn = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
		self.total_time = 0.0
		for i in xrange(self.msgcount):
			time_start = time.time()
			conn.sendmail(MSG_FROM, MSG_TO, MSG_DATA);
			time_end = time.time()
			self.total_time += time_end - time_start
		conn.quit()
		self.msg_avg = self.total_time / self.msgcount
		self.mps = 1.0 / self.msg_avg

def ThreadBenchmark(thread_count):
	print 'Benchmarking Multiple Sessions (Threaded)'
	print ' - Thread Count:', thread_count
	
	threads = []
	thread_msgcount = int(MSG_COUNT / thread_count)
	
	print ' - Per Thread Message Count:', thread_msgcount
	print ' - Total Message Count:', thread_msgcount * thread_count
	
	for i in xrange(thread_count):
		threads.append(ThreadBench(thread_msgcount))
		
	time_start = time.time()
	for t in threads:
		t.start()
	for t in threads:
		t.join()
	time_end = time.time()
	
	total_time = time_end - time_start
	
	accum_time = 0.0
	avg_mps = 0.0
	avg_msg = 0.0
	for t in threads:
		accum_time += t.total_time
		avg_mps += t.mps
		avg_msg += t.msg_avg
	avg_mps = avg_mps / thread_count
	avg_msg = avg_msg / thread_count
	mps = (thread_msgcount * thread_count) / total_time
	msg_time = 1.0 / mps
	total_data = (thread_msgcount * thread_count) * MSG_SIZE
	print ' * Total Time:', total_time
	print ' * Accumulated Time:', accum_time
	print ' * TT/AT Ratio:', total_time / accum_time
	print ' * Time per Message:', msg_time
	print ' * Messages per Second:', mps
	print ' * Avg. Thread Time per Message:', avg_msg
	print ' * Avg. Thread Message per Second:', avg_mps
	print ' * Total Message Data Transmited:', total_data, 'bytes'
	print ' * Data Rate:', total_data / total_time, 'bytes/second'
	print ' * Per Thread Data:'
	for i in xrange(thread_count):
		t = threads[i]
		print '  - Thread', i
		print '    * Total Time:', t.total_time
		print '    * Avg. Time per message:', t.msg_avg
		print '    * Messages per Second:', t.mps

print 'FancyMail Simple SMTP Benchmark Suite'
print '  Params:'
print '   * SMTP Port:', SMTP_PORT
print '   * SMTP Host:', SMTP_HOST
print '   * Message Count:', MSG_COUNT
print '   * Message Size:', MSG_SIZE

print ''

SingleSession()

print ''

ConnectionBench()

print ''

for i in THREAD_COUNT:
	ThreadBenchmark(i)
	print ''
