/*
libfmail: Generic Search Tree implementation

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* Generic Search Tree
 * For a given key string and value, generate
 * a tree that can be navigated using the key
 * characters to retrieve a value.
 *
 * Given the Key-Value:
 *  * test-20
 *  * key-8
 *  * some-10
 *  * same-11
 *   ROOT
 *  / | \
 * t  k  s
 * e  e  o \
 * s  y  m  a
 * t     e  m
 *          e
 * The search tree may be arranged this way, with the last
 * node having the value.
 *
 * Notes:
 *  * This algorithm is O(n), may be better than binary trees but not
 *    so good as hash tables, proable is in the middle between speed
 *    and memory usage.
 *  * This algorithm may consume a lot of memory, depending on the
 *    keys, it is recomended to use a hash function with values ranging
 *    from 0 to 26 for best performance.
 * */

typedef int (*HASH_FUNC)(char s);

/* Search failure exception */
class SearchNotFound {};

/* Node class */
template <class T, int max> class TreeNode{
public:
    T value;
    TreeNode<T, max> *tree[max];
    
    TreeNode(){
        for (int i = 0; i < max; i++){
            tree[i] = NULL;
        }
    }
    ~TreeNode(){
        int i;
        //printf("Deleting Node\n");
        for (i = 0; i < max; i++){
            if (tree[i] != NULL){
                //printf("\tDeleting Child\n");
                delete tree[i];
            }
        }
    }
};

/* Generic search tree class 
 * may hold T type of values, may use
 * max number for characters for key hashes,
 * may use hash_func for character key hashes */
template <class T, int max, HASH_FUNC hash_func> class SearchTree{
private:
    TreeNode<T, max> *rootNode;

	int getNodeSize(TreeNode<T, max> *node){
		int size;
		int i;

		size = 0;
		size += sizeof(T); //Size of value
		size += sizeof(void*) * max;
		for (i = 0; i < max; i++){
			if (node->tree[i] != NULL)
				size += getNodeSize(node->tree[i]);
		}

		return size;
	}
public:
    SearchTree(){
        rootNode = new TreeNode<T, max>();
    }
    ~SearchTree(){
        delete rootNode;
        rootNode = NULL;
    }
    int Insert(char *key, T value){
        int i, l, tk;
        TreeNode<T, max> *c;
        
		/* Grab root node and key length*/
        c = rootNode;
        l = strlen(key);
        
		/* For each character in key insert a node */
        for (i = 0; i < l; i++){
			/* Get the hash key character */
            tk = hash_func(key[i]);

			/* If hash key character > max allowed should fail, undefined for now */
            if (tk > max)
                printf("GOT MAX\n");
            
			/* If next node is NULL create */
            if (c->tree[tk] == NULL)
                c->tree[tk] = new TreeNode<T, max>();
            c = c->tree[tk];
        }
		/* Once we reach the final node, set the value */
        c->value = value;
        return 0;
    }
    T Lookup(char *key){
        int i, l, tk;
        TreeNode<T, max> *c;
        
        c = rootNode;
        l = strlen(key);
        
        for (i = 0; i < l; i++){
            tk = hash_func(key[i]);
            if (tk > max)
                throw SearchNotFound();
            
            if (tk == -1)
                break;
                
            if (c->tree[tk] == NULL)
                throw SearchNotFound();
                
            c = c->tree[tk];
        }
        
        return c->value;
    }

	int getTreeSize(){
		return getNodeSize(rootNode);
	}

    int HasKey(char *key){
        int i, l, tk;
        TreeNode<T, max> *c;
        
        c = rootNode;
        for (i = 0; i < l; i++){
            tk = hash_func(key[i]);
            
            if (tk > max)
                return 0;
            
            if (tk == -1)
                break;
                
            if (c->tree[tk] == NULL)
                return 0;
                
            c = c->tree[tk];
        }
        
        return 1;
    }

};
