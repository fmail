/*
   FancyMail: Queue Manager

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* TODO: Add multiple Queues support */

#include <libfmail.h>
#include <libfmail/socketipc.h>

#include <pcrecpp.h>

#include <time.h>

int knhash(char v){
	if ((v >= 'A') && (v <= 'Z'))
		return v - 'A';

	if (v == '_')
		return 26;

	if ((v >= 'a') && (v <= 'z'))
		return v - 'a';

	if (v == ' ')
		return -1;

	if (v == 0x0D)
		return -1;

	return 255;
}

class Subscriber {
	std::string subs_uri;
public:
	Subscriber(const char * uri){
		this->subs_uri = uri;
	}

	void Alert(char * qname){
		IPC * ipc;
		IPCMessage * msg;

		ipc = IPC::CreateIPC((char *)subs_uri.c_str());
		ipc->RequestIPC();

		msg = new IPCMessage("alert");
		msg->PushParam(qname);
		ipc->SendMessage(msg);

		delete msg;
		ipc->CloseIPC();
		delete ipc;
	}
};

/* TODO: Change to state based machine, add error handling */
class QueueHandler : public ProtocolHandler {
	Semaphore qlock;/* Message Queue Semaphore*/
	queue<string *> msg_queue;/* Message Queue */
	Semaphore slock;/* Subscriber Queue Semaphore */
	queue<Subscriber *> subs_queue;/* Subscribe Queue */
	char path[100];
public:
	QueueHandler(const char * qpath, const char * name){
		/* Initialize Queue Path */
		sprintf(path, "%s/%s/", qpath, name);

		/* Initialize semaphore value to 1 */
		qlock.Post();
	}

	int Handle(Socket * s){
		char buffer[255], * queue_name;
		int onrun, r, ret;
		IPC * ipc;
		IPCMessage * msg;
		time_t ctime;
		struct tm * gtime;
		string * slot;

		onrun = 1;

		/* Start IPC in the socket */
		ipc = new SocketIPC(s);

		while (onrun){
			/* Get our message */
			msg = ipc->ReciveMessage();

			if (msg == NULL){
				onrun = 0;
				break;
			}

			if (!strcmp(msg->GetMessageName(), "slot")){
				/* TODO: check that queue exists */
				/* We recived a slot request, get for which queue*/
				queue_name = msg->PopParam();
				free(queue_name);

				/* Get current time*/
				time(&ctime);
				gtime = gmtime(&ctime);

				/* Generate a Random number */
				r = rand();

				/* Construct a unique filename for the slot */
				sprintf(buffer, "%s%i%i%i%i%i", path, gtime->tm_yday, gtime->tm_hour, gtime->tm_min, gtime->tm_sec, r);

				/* Send positive response, with the new slot name */
				msg = new IPCMessage("ok");
				msg->PushParam(buffer);
				ipc->SendMessage(msg);
				delete msg;

			} else if (!strcmp(msg->GetMessageName(), "push")){
				/* Get the slot name */
				slot = new string(msg->PopParam());

				/* Decrement semaphore */
				qlock.Wait();

				/* Push the slot to the queue */
				msg_queue.push(slot);

				/* Increment semaphore */
				qlock.Post();

				delete msg;
			} else if (!strcmp(msg->GetMessageName(), "pop")){
				/* Decrement semaphore */
				qlock.Wait();

				/* Check if there is anything in the queue */
				if (msg_queue.size() == 0){
					ret = 0;
				} else{
					/* If there is messages in the queue pop them */
					slot = msg_queue.front();
					msg_queue.pop();
					ret = 1;
				}

				/* Increment sempahore */
				qlock.Post();

				delete msg;
				if (ret){
					/* Send a positive response with the message filename */
					msg = new IPCMessage("ok");
					msg->PushParam((char *)slot->c_str());
				} else{
					/* There is no messages in the queue, return error */
					msg = new IPCMessage("error");
				}
				ipc->SendMessage(msg);
				delete msg;
			} else if (!strcmp(msg->GetMessageName(), "quit")){
				/* End Session */
				onrun = 0;
				delete msg;
			} else if (!strcmp(msg->GetMessageName(), "subscribe")){
				//Subscriber();
			} else if (!strcmp(msg->GetMessageName(), "unsubscribe")){
			}
		}
		s->Close();

		return 0;
	}
};

class SimpleLoad : public LoadHandler {
public:
	int Dispatch(Socket * sock, ProtocolHandler * ph){
		if (ph)
			return ph->Handle(sock);
		return 0;
	}
};

int main(){
	LoadHandler * lh;
	QueueHandler * handler;
	Socket * s, * cl;
	int ret, port, tcount;
	std::string conf_handler, qname, qpath;
	Configuration conf("queue.conf");

	/* Load configuration */
	port = conf.getInt("port", 14001);
	conf_handler = conf.getString("loadhandler", "simple");

	/* Create socket and bind to QUEUE port */
	s = Socket::CreateSocket(SOCKET_INET, 0);
	s->setAddress("127.0.0.1");
	s->setPort(port);


	if (s->Bind()){
		printf("Error: Unable to bind to port\n");
		return 0;
	}

	s->Listen(15);

	/* Read Queue configuration */
	qname = conf.getString("queue.name", "incoming");
	qpath = conf.getString("queue.path", "/tmp/fmail/queue");

	/* Create our Queue handler and Threaded LoadHandler */
	handler = new QueueHandler(qpath.c_str(), qname.c_str());

	if (conf_handler == "thread"){
		tcount = conf.getInt("thread.count", 10);
		lh = new ThreadLoad(handler, tcount);
	} else{
		lh = new SimpleLoad();
	}

	/* TODO: Exit gracefuly, don't serve forever. */
	while(1){
		/* Wait for incoming connections */
		ret = s->Poll(1000000, SOCKET_POLL_READ | SOCKET_POLL_ERROR);

		if (ret & SOCKET_POLL_READ){
			/* Accept client and dispatch */
			cl = s->Accept();
			lh->Dispatch(cl, handler);
		} else if(ret & SOCKET_POLL_ERROR){ //error
			return -1;
		} else{ //timeout
			break;
		}
	}

	return 0;
}
