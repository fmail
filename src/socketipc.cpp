/*
   libfmail: Socket Based IPC mechanism

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <pcrecpp.h>

#include <queue>

#include <libfmail/socket.h>
#include <libfmail/ipcmsg.h>
#include <libfmail/ipc.h>
#include <libfmail/socketipc.h>


/*	SocketIPC State Machine */
class SISM {
public:
	int state;
	int opts;

	char * buffer;
	int blen;
	int bpos;

	char * obuffer;
	int olen;
	int opos;

	SISM(){
		state = 0;
		buffer = obuffer = NULL;
		bpos = blen = 0;
		olen = opos = 0;
	}

	void setBuffer(char * buf, int len){
		buffer = buf;
		blen = len;
		bpos = 0;
	}

	void setOBuffer(char * buf, int len){
		obuffer = buf;
		olen = len;
		opos = 0;
	}

	int Parse(){
		int onrun;

		onrun = 1;

		while(onrun){
			if ((bpos > blen) || (buffer == NULL))
				return -1;

			switch(state){
			case 0:/* PARSES: MSG[ */
				/* Once we reached the data marker change state */
				if (buffer[bpos] == '['){
					state = 1;
					onrun = 0;
				}
				break;
			case 1:/* PARSES: \d] */
				if (buffer[bpos] == ']'){
					state = 2;
					onrun = 0;
				} else{
					/* Copy character to our temporary buffer */
					obuffer[opos] = buffer[bpos];
					opos++;
				}
			case 2:/* PARSES: [ */
				/* Once we reached the data marker change state */
				if (buffer[bpos] == '['){
					state = 3;
					onrun = 0;
				}
				break;
			case 3:/* PARSES: \d] */
				if (buffer[bpos] == ']'){
					state = 4;
					onrun = 0;
				} else{
					/* Copy character to our temporary buffer */
					obuffer[opos] = buffer[bpos];
					opos++;
				}
				break;
			case 4:/* Reads l chars */
				/* Copy character to our temporary buffer */
				obuffer[opos] = buffer[bpos];
				opos++;

				if (opos == olen){
					state = 5;
					onrun = 0;
				}
				break;
			case 5:/* Check Param Count */
				if (olen == 0){
					state = 10;
				} else{
					state = 6;
				}
				onrun = 0;
				break;
			case 6:/* PARSES: PARAM[ */
				/* Once we reached the data marker change state */
				if (buffer[bpos] == '['){
					state = 7;
				}
				break;
			case 7:/* PARSES: \d] */
				if (buffer[bpos] == ']'){
					state = 8;
					onrun = 0;
				} else{
					/* Copy character to our temporary buffer */
					obuffer[opos] = buffer[bpos];
					opos++;
				}
				break;
			case 8:/* Reads l chars */
				/* Copy character to our temporary buffer */
				obuffer[opos] = buffer[bpos];
				opos++;

				if (opos == olen){
					state = 9;
					onrun = 0;
				}
				break;
			default:
				onrun = 0;
				break;
			}

			bpos++;
			if (opos > olen)
				return -2;
		}

		return state;
	}
};

SocketIPC::SocketIPC(Socket * s){
	sock = s;
}

/* Create a socket IPC from uri, create
 * sockets and parse host and port. */
SocketIPC::SocketIPC(char * uri){
	string shost;
	string options;
	pcrecpp::RE re("([\\w\\d\\.]+)\\:(\\d*)[/]*(\\w*)");

	re.PartialMatch(uri, &shost, &port, &options);
	host = shost.c_str();

	sock = Socket::CreateSocket(SOCKET_INET, 0);
	sock->setPort(port);
	sock->setAddress(host);
	auxsock = NULL;
}

/* Connect to remote process for IPC */
int SocketIPC::RequestIPC(){
	int ret;
	int i;

	for (i = 0; i < 20; i++){
		ret = sock->Connect();
		if (ret == 0)
			break;
	}

	return ret;
}

/* Wait for incoming IPC */
int SocketIPC::ListenIPC(){
	sock->Bind();
	sock->Listen(10);

	auxsock = sock->Accept();
	return 0;
}

/* Close IPC Session */
int SocketIPC::CloseIPC(){
	sock->Close();
	if (auxsock)
		auxsock->Close();
	return 0;
}

/* Lousy Parser, just works
 * TODO: May not work with fragmented buffers.
 * TODO: Write something more robust.
 * */
int SocketIPC::ParseLoose(){
	char buffer[255], abuffer[255];
	int ret, state, onParse;
	int l, r, argc, argn;
	IPCMessage * msg = NULL;
	SISM parser;

	//printf("Loose Parsing\n");

	/* Initialize our state variables */
	state = 0;
	onParse = 1;
	argc = 0;
	argn = 0;
	l = 0;

	while(onParse){
		switch (state){
		case -1:
		case 0:
			ret = sock->Poll(100000, SOCKET_POLL_READ | SOCKET_POLL_WRITE | SOCKET_POLL_ERROR);

			if (ret & SOCKET_POLL_READ){
				memset(buffer, 0, 255);
				r = sock->Read(buffer, 255);

				if (r > 0)
					parser.setBuffer(buffer, r);
			} else{
				return 0;
			}
			break;
		case 1:
			//printf("S1\n");
			memset(abuffer, 0, 255);
			parser.setOBuffer(abuffer, 255);
			break;
		case 2:
			l = atoi(abuffer);
			//printf("S2: %i\n", l);
			memset(abuffer, 0, 255);
			parser.setOBuffer(abuffer, 255);
			break;
		case 4:
			argc = atoi(abuffer);
			//printf("S4: %i\n", argc);
			memset(abuffer, 0, 255);
			parser.setOBuffer(abuffer, l);
			break;
		case 5:
			msg = new IPCMessage(abuffer);
			//printf("New IPC Message: %s\n", abuffer);
			if (argc){
				memset(abuffer, 0, 255);
				parser.setOBuffer(abuffer, 255);
				argn++;
			} else{
				parser.setOBuffer(NULL, 0);
			}
			break;
		case 8:
			l = atoi(abuffer);
			memset(abuffer, 0, 255);
			parser.setOBuffer(abuffer, l);
			break;
		case 9:
			msg->PushParam(abuffer);
			//printf("\tParam: %s\n", abuffer);
			parser.state = 5;
			if (argn < argc){
				memset(abuffer, 0, 255);
				parser.setOBuffer(abuffer, 255);
				argn++;
			} else{
				parser.setOBuffer(NULL, 0);
			}
			break;
		case 10:
			onParse = 0;
			parser.state = -1;
			break;
		default:
			break;
		}
		state = parser.Parse();
	}

	if (msg != NULL){
		msgQueue.push(msg);
		return 1;
	}
	return 0;
}

int SocketIPC::PeekMessage(){
	if(ParseLoose())
		return 1;

	if (msgQueue.size() > 0)
		return 1;

	return 0;
}

int SocketIPC::SendMessage(IPCMessage * msg){
	return PushMessage(msg);
}

IPCMessage * SocketIPC::ReciveMessage(){
	IPCMessage * msg;

	//printf("IPC: Recive Message\n");
	//printf("\tmsgQueue Size: %i\n", msgQueue.size());

	if (msgQueue.size() > 0){
		//printf("\tMSG Already on Queue\n");
		msg = msgQueue.front();
		msgQueue.pop();
		return msg;
	}

	//printf("\tPolling for Incoming Message\n");

	/* TODO: Add a timeout or max try limit */
	while(msgQueue.size() < 1){
		ParseLoose();
	}

	//printf("\tmsgQueue Size: %i\n", msgQueue.size());

	msg = msgQueue.front();
	msgQueue.pop();

	//printf("IPC: Message Retrieved\n");
	return msg;
}

char *makemsg(IPCMessage * msg, int * r){
	char * ret, * tmp;
	int len, i, j;
	size_t offset;

	len = 40;

	len += msg->GetPayload();
	len += msg->ParamCount() * 15;

	ret = (char *)malloc(sizeof(char) * len);
	if (!ret)
		return 0;
	sprintf(ret, "MSG[%i][%i]%s", strlen(msg->GetMessageName()), msg->ParamCount(), msg->GetMessageName());

	offset = strlen(ret);
	j = msg->ParamCount();

	for (i = 0; i < j; i++){
		tmp = msg->PopParam();
		len = strlen(tmp);
		if (len < 10){
			len++;
		} else if(len < 100){
			len += 2;
		} else if(len < 1000){
			len += 3;
		}
		len += 7;

		sprintf((char *)((int)ret+offset), "PARAM[%i]%s", strlen(tmp), tmp);
		offset += len;
		free(tmp);
	}

	*r = offset;
	return ret;
}

/* TODO: Better memory management for IPCMessage */
int SocketIPC::PushMessage(IPCMessage * msg){
	char * tmp;
	int l;

	tmp = makemsg(msg, &l);
	//printf("IPCMSG: %s\n", tmp);

	sock->Write(tmp, l);
	free(tmp);

	return 0;
}

IPCMessage * SocketIPC::PopMessage(){
	IPCMessage * msg;

	msg = msgQueue.front();
	msgQueue.pop();

	return msg;
}

int SocketIPC::RawRead(char * buff, int size){
	return sock->Read(buff, size);
}

int SocketIPC::RawWrite(char * buff, int size){
	return sock->Write(buff, size);
}

