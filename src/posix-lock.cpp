/*
   libfmail: Simple Lock class

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <pthread.h>
#include <libfmail/lock.h>

struct _oslock {
	pthread_mutex_t lock;
};

Lock::Lock(){
	os_lock = (OSLock *)malloc(sizeof(OSLock));
	pthread_mutex_init(&(os_lock->lock), NULL);
}

Lock::~Lock(){
	pthread_mutex_destroy(&(os_lock->lock));
	free(os_lock);
}

int Lock::Acquire(){
	return pthread_mutex_lock(&(os_lock->lock));
}

int Lock::Release(){
	return pthread_mutex_unlock(&(os_lock->lock));
}

int Lock::TryAcquire(){
	return pthread_mutex_trylock(&(os_lock->lock));
}


