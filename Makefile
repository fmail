# This is the Makefile for fmail.
# Copyright (C) WANG Cong
# GPLv2 applied.

CC= g++
CFLAGS= -Wall -W -I./include -g
.PHONY: clean all debug

all: bin/libfmail.so bin/fmail-fsauth bin/fmail-fsmailbox bin/fmail-queue bin/fmail-pop3 bin/fmail-smtp

bin/baseserver.o: src/baseserver.cpp
	mkdir -p bin
	$(CC) $^ $(CFLAGS) -c -o $@
bin/socket.o: src/posixsocket.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/ipcmsg.o: src/ipcmsg.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/socketipc.o: src/socketipc.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/ipc.o: src/ipc.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/thread.o: src/posix-thread.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/lock.o: src/posix-lock.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/semaphore.o: src/posix-sem.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/threadpool.o: src/threadpool.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/configuration.o: src/configuration.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/worker.o: src/worker.cpp
	$(CC) $^ $(CFLAGS) -c -o $@
bin/libfmail.so: bin/baseserver.o bin/socket.o bin/socketipc.o bin/ipc.o bin/ipcmsg.o bin/lock.o bin/thread.o bin/threadpool.o bin/semaphore.o bin/configuration.o bin/worker.o
	$(CC) $^ -lpthread -lpcrecpp -lstdc++ -shared -o $@

#echo "Compiling Testcases"
#$(CC) testcase/ipctest.cpp -lfmail -g -I./include -L./bin -o bin/ipctest
#$(CC) testcase/ipccommand.cpp -lfmail -g -I./include -L./bin -o bin/ipccommand

bin/fmail-fsauth: backends/auth/fsauth.cpp
	$(CC) $^ -lstdc++  -L./bin -lfmail $(CFLAGS) -o $@
bin/fmail-fsmailbox: backends/mailbox/fsmailbox.cpp
	$(CC) $^ -lstdc++ -L./bin -lfmail $(CFLAGS) -o $@
bin/fmail-queue: backends/queue/queueman.cpp
	$(CC) $^ -lstdc++ -L./bin -lfmail $(CFLAGS) -o $@
bin/fmail-pop3: backends/protocol/pop3.cpp
	$(CC) $^ -L./bin -lfmail -lstdc++ -lpcrecpp $(CFLAGS) -o $@
bin/fmail-smtp: backends/protocol/smtp.cpp
	$(CC) $^ -L./bin -lfmail -lstdc++ -lpcrecpp $(CFLAGS) -o $@

clean:
	rm -fr bin
debug: bin/libfmail.so bin/fmail-fsauth.debug bin/fmail-fsmailbox.debug bin/fmail-queue.debug bin/fmail-pop3.debug bin/fmail-smtp.debug


bin/fmail-fsauth.debug: backends/auth/fsauth.cpp
	$(CC) $^ -lstdc++  -L./bin -lfmail -ggdb $(CFLAGS) -o $@
bin/fmail-fsmailbox.debug: backends/mailbox/fsmailbox.cpp
	$(CC) $^ -lstdc++ -L./bin -lfmail -ggdb $(CFLAGS) -o $@
bin/fmail-queue.debug: backends/queue/queueman.cpp
	$(CC) $^ -lstdc++ -L./bin -lfmail -ggdb $(CFLAGS) -o $@
bin/fmail-pop3.debug: backends/protocol/pop3.cpp
	$(CC) $^ -L./bin -lfmail -lstdc++ -lpcrecpp -ggdb $(CFLAGS) -o $@
bin/fmail-smtp.debug: backends/protocol/smtp.cpp
	$(CC) $^ -L./bin -lfmail -lstdc++ -lpcrecpp -ggdb $(CFLAGS) -o $@

