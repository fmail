/*
libfmail: Basic Socket Server

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <libfmail.h>

BaseServer::BaseServer(ProtocolHandler *ph, int p){
	sock = Socket::CreateSocket(SOCKET_INET, 0);
	sock->setPort(p);
	lh = NULL;
	proto = ph;
}

int BaseServer::SetLoadhandler(LoadHandler *l){
	lh = l;
	return 0;
}

int BaseServer::Listen(){
	printf("Setting up server listener\n");
	printf(" * Binding to port\n");
	sock->Bind();
	
	printf(" * Listening Queue\n");
	sock->Listen(10);
    
	return 1;
}

int BaseServer::Accept(){
	Socket *client;
	
	printf(" * Got client\n");
	printf(" * Accepting\n");
	client = sock->Accept();
	
	printf("Got client, dispatching!\n");
	if (lh)
		lh->Dispatch(client, proto);
	printf("Client Served listening for more\n");
	
	return 1;
}

