/*
libfmail: Thread Worker Pattern Implementation

Copyright (C) 2008  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

class Job{
private:
public:
    virtual ~Job(){}
    virtual int Run() = 0;
};

class Worker : public Thread, public Lock {
private:
    Job *cJob;
    Lock *mtx;
    int onRun;
    bool autodel;
public:
    Worker();
    ~Worker();
    int setJob(Job *job, bool autodelete);
    int wakeup();
    int Run();
};

class Boss{
private:
    Worker **workers;
    int nworkers;
public:
    Boss(int n);
    ~Boss();
    int Dispatch(Job *jb, bool autodelete = true);
};

