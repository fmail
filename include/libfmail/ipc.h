/*
libfmail: IPC mechanism

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

class IPC{
private:
public:
	static IPC *CreateIPC(const char *ipc_uri);
	virtual ~IPC(){}    
	virtual int RequestIPC() = 0;
	virtual int ListenIPC() = 0;
	virtual int CloseIPC() = 0;

	virtual int PeekMessage() = 0;

	virtual IPCMessage *ReciveMessage() = 0;
	virtual int SendMessage(IPCMessage *msg) = 0;

	virtual int PushMessage(IPCMessage *msg) = 0;
	virtual IPCMessage *PopMessage() = 0;

	virtual int RawRead(char *buffer, int size) = 0;
	virtual int RawWrite(char *buffer, int size) = 0;
};
