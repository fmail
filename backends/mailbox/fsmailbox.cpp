
#include <libfmail.h>
#include <libfmail/socketipc.h>
#include <dirent.h>

enum {MSG_LIST = 1, MSG_STORE = 2, MSG_RETR = 3};

int knhash(char v){
	if ((v >= 'A') && (v <= 'Z'))
		return v - 'A';

	if ((v >= 'a') && (v <= 'z'))
		return v - 'a';
	
	return 255;
}

class FSMailbox : public ProtocolHandler{
private:
	SearchTree<int, 26, &knhash> cmdtree;
public:
	FSMailbox(){
		cmdtree.Insert("list", MSG_LIST);
		cmdtree.Insert("store", MSG_STORE);
		cmdtree.Insert("retr", MSG_RETR);
	}
	int Handle(Socket *s){
		IPCMessage *msg;
		IPC *ipc;
		int onRun, msgid, n, i, len;
		char *user, *rcpt, *msglen, buffer[255];
		FILE *fd;
		//DIR *dir;
		struct dirent **dent;

		printf("Got Client\n");
		printf("Creating new IPC\n");

		ipc = new SocketIPC(s);
		onRun = 1;

		while(onRun){
			printf("Waiting for Message\n");
			if (ipc->PeekMessage()){
				printf("Got Message\n");
				msg = ipc->PopMessage();
				try{
					msgid = cmdtree.Lookup((char*)msg->GetMessageName());
				}catch (SearchNotFound){
					msgid = -1;
				}
				switch(msgid){
				case MSG_LIST:
					user = msg->PopParam();
					sprintf(buffer, "/tmp/fmail/%s/", user);
					n = scandir(buffer, &dent, NULL, alphasort);
					sprintf(buffer, "%i", n);
					msg = new IPCMessage("ok");
					msg->PushParam(buffer);
					ipc->PushMessage(msg);
					for (i = 0; i < n; i++){
						msg = new IPCMessage("mailinfo");
						msg->PushParam(dent[i]->d_name);
						free(dent[i]);
					}
					free(dent);
					break;
				case MSG_STORE:
					user = msg->PopParam();
					rcpt = msg->PopParam();
					msglen = msg->PopParam();
					//len = atoi(msglen);
					len = 1025;
					fd = fopen("/tmp/fmail/in/1", "w");
					n = 0;
					while(n < len){
						i = ipc->RawRead(buffer, 255);
						fwrite(buffer, 1, i, fd);
						if (i == 0)
							break;
						n += i;
					}
					fclose(fd);
					break;
				case MSG_RETR:
					break;
				case 4:
					break;
				default:
					break;
				}
			}
		}
		ipc->CloseIPC();
		delete ipc;
		return 0;
	}
};

class SimpleLoad : public LoadHandler{
public:
	int Dispatch(Socket *sock, ProtocolHandler *ph){
		printf("SimpleLoad: Dispatching\n");
		if (ph)
			return ph->Handle(sock);
		return 0;
	}
};

int main(){
	//BaseServer *serv;
	LoadHandler *lh;
	FSMailbox *handler;
	Socket *s, *client;
	int r;

	s = Socket::CreateSocket(SOCKET_INET, 0);
	s->setAddress("127.0.0.1");
	s->setPort(14001);

	s->Bind();
	s->Listen(10);

	printf("Creating new FSMailbox\n");
	handler = new FSMailbox();

	printf("Creating new SimpleLoad\n");
	lh = new SimpleLoad();

	while (1){
		r = s->Poll(10000, SOCKET_POLL_READ);
		if (r == SOCKET_POLL_READ){
			printf("Got new client!\n");
			client = s->Accept();
			lh->Dispatch(client, handler);
		}
	}

	return 0;
}
