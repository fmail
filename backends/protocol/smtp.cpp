/*
   SMTP Protocol Handler

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <time.h>
#include <signal.h>

#include <libfmail.h>
#include <pcrecpp.h>

const char CRLF[2] = { 0x0D, 0x0A };

static bool sRun;

/* Currently supported SMTP commands */
const int CMD_EHLO = 1;
const int CMD_HELO = 2;
const int CMD_MAIL = 3;
const int CMD_RCPT = 4;
const int CMD_DATA = 5;
const int CMD_QUIT = 6;
const int CMD_RSET = 7;
const char KEY_END_DATA[6] = { 0x0D, 0x0A, '.', 0x0D, 0x0A, 0x00 };

/* Create a hash number from a character for search tree */
int knhash (char v) {
	if ((v >= 'A') && (v <= 'Z'))
		return v - 'A';

	if (v == '_')
		return 26;

	if ((v >= 'a') && (v <= 'z'))
		return v - 'a';

	if (v == ' ')
		return -1;

	if (v == 0x0D)
		return -1;

	return 255;
}

int genPath(char *buffer){
    time_t ctime;
    struct tm * gtime;
    int r;

    /* Get current time*/
    time(&ctime);
    gtime = gmtime(&ctime);
    
    /* Generate a Random number */
    r = rand();

    /* Construct a unique filename for the slot */
    sprintf(buffer, "/tmp/fmail/queue/incoming/%i%i%i%i%i", gtime->tm_yday, gtime->tm_hour, gtime->tm_min, gtime->tm_sec, r);
    return 1;
}


class SMTPHandler : public Job {
	SearchTree < int, 27, &knhash > cmdtree;
	std::string queue_ipc;
    Socket *s;
public:
	SMTPHandler (std::string qipc, Socket *sock) : Job() {
		/* Queue IPC string */
		//queue_ipc = qipc;

        s = sock;

		/* Insert the SMTP commands to search tree */
		cmdtree.Insert ("EHLO", 1);
		cmdtree.Insert ("HELO", 2);
		cmdtree.Insert ("MAIL", 3);
		cmdtree.Insert ("RCPT", 4);
		cmdtree.Insert ("DATA", 5);
		cmdtree.Insert ("QUIT", 6);
		cmdtree.Insert ("RSET", 7);
	}
    ~SMTPHandler(){
        delete s;
    }
	int Run () {
		char buffer[255], * slotname;
		int r, l, ret;
		int onrun, cmd, state;
		FILE * fd;
		pcrecpp::RE reCmd ("\\w*:<(.*)>");
		state = 0;
		onrun = 1;
		cmd = 0;
		string mailfrom, rcpt;

		/* Write out the SMTP server Greeting */
		s->Write ("220 FancyMail v0.1", 18);
		s->Write (CRLF, 2);

		while (onrun) {
			/* Wait for client input */
			memset (buffer, 0, 255);
			r = s->Read (buffer, 255);

			/* Find the given command on our search tree */
			try {
				cmd = cmdtree.Lookup (buffer);
			} catch (SearchNotFound) {
				cmd = -1;
			}

			switch (cmd) {
			case CMD_HELO:
				/* We got normal SMTP HELO, write response */
				s->Write ("250 ok localhost", 16);
				s->Write (CRLF, 2);

				/* Client presented itself, we can accept other
				 * commands, we pass to state 1 */
				state = 1;
				break;
			case CMD_EHLO:
				/* We still not support ESMTP, write not implemented */
				s->Write ("502 Not implemented", 19);
				s->Write (CRLF, 2);
				break;
			case CMD_MAIL:
				/* Check if we had the proper greeting */
				if (state != 1) {
					s->Write ("502 Not implemented", 19);
					s->Write (CRLF, 2);
					break;
				}

				/* Extract the command mail from data */
				ret = reCmd.PartialMatch (buffer, &mailfrom);

				/* Write Response */
				s->Write ("250signal(SIGTERM, SIG_IGN); OK", 6);
				s->Write (CRLF, 2);

				/* We pass to next state */
				state = 2;
				break;
			case CMD_RCPT:
				/* Check that we have recieved the MAIL command first */
				if (state != 2) {
					s->Write ("502 Not implemented", 19);
					s->Write (CRLF, 2);
					break;

				}

				/* Extract the recipent */
				reCmd.PartialMatch (buffer, &rcpt);

				s->Write ("250 OK", 6);
				s->Write (CRLF, 2);

				state = 3;
				break;
			case CMD_DATA:
				/* Check that we have recived the RCPT command first */
				if (state != 3) {
					s->Write ("502 Not implemented", 19);
					s->Write (CRLF, 2);
					break;
				}

				/* Give the client green light to send its data */
				s->Write ("354 OK", 6);
				s->Write (CRLF, 2);
                
                genPath(buffer);
                slotname = buffer;

				/* Open Queue Slot File */
				fd = NULL;
				fd = fopen (slotname, "w");


				if (fd == NULL) {
					printf ("Error: Unable to open destination SLOT\n");
					onrun = 0;
					break;
				}

				/* Write our headers */
				fprintf (fd, "MAIL FROM: %s\n", mailfrom.c_str ());
				fprintf (fd, "RCPT: %s\n", rcpt.c_str ());
				fprintf (fd, "\n");

				r = 1;
				while (r) {
					/* Read incoming client data */
					memset (buffer, 0, 255);
					l = s->Read (buffer, 255);

					/* Write to slot file */
					fwrite (buffer, 1, l, fd);

					/* Check for EOF */
					if (strstr (buffer, KEY_END_DATA)) {
						r = 0;
					}
				}

				/* Close our slot file */
				fclose (fd);

				s->Write ("250 OK", 6);
				s->Write (CRLF, 2);

				/* Go back to initial state */
				state = 1;
				break;
			case CMD_RSET:
				s->Write ("250 OK", 6);
				s->Write (CRLF, 2);

				/* Reset state */
				state = 1;
				break;
			case CMD_QUIT:
				s->Write ("221 Exiting", 11);
				s->Write (CRLF, 2);

				/* Stop main loop */
				onrun = 0;
				break;
			default:
				printf ("Not implemented [State: %i]: %s\n", state, buffer);
				s->Write ("502 Not implemented", 19);
				s->Write (CRLF, 2);
				break;
			}

		}

		s->Close ();
        
		return 0;
	}
};

class SimpleLoad : public LoadHandler {
public:
	int Dispatch (Socket * sock, ProtocolHandler * ph) {
		if (ph)
			return ph->Handle (sock);
		return 0;
	}
};

void onExit(int sig){
    printf("Got Signal %i\n", sig);
    sRun = false;
    signal(SIGTERM, SIG_IGN);
    signal(SIGINT, SIG_IGN);
}

int main () {
	Socket * s, * cl;
    Boss *boss;
	int ret, port, tcount;
	std::string conf_handler, qipc;
	Configuration conf ("smtp.conf");
    struct sigaction act;
    
    act.sa_handler = onExit;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    
    sigaction(SIGINT, &act, 0);
    
	/* Get basic configuration options */
	port = conf.getInt ("port", 25);
	conf_handler = conf.getString ("loadhandler", "simple");

	/* Create a Socket and bind it to SMTP port */
	s = Socket::CreateSocket (SOCKET_INET, 0);
	s->setAddress (NULL);
	s->setPort (port);

	if (s->Bind ()) {
		printf ("Unable to bind to port\n");
		return 0;
	}

	s->Listen (15);

	/* Load queue connection string */
	qipc = conf.getString ("queue", "socket://127.0.0.1:14003");

	/* Create a Boss for Thread Worker based on configuration */
	if (conf_handler == "thread") {
		/* If we got threaded loadhandler check for max worker thread count */
		tcount = conf.getInt ("thread.count", 10);
        boss = new Boss(tcount);
	} else {
		boss = new Boss(1);
	}

	sRun = true;
    
	while (sRun) {
		/* Poll for incoming connections */
		ret = s->Poll (1000000, SOCKET_POLL_READ | SOCKET_POLL_ERROR);
		if (ret & SOCKET_POLL_READ) {
			/* Accept client and dispatch */
			cl = s->Accept ();
            boss->Dispatch(new SMTPHandler (qipc, cl));
		} else if (ret & SOCKET_POLL_ERROR) { //error
			return -1;
		} else {    //timeout
			break;
		}
	}
    
    printf("fmail-smtp shutting down\n");
    s->Close();
    
    delete s;
    delete boss;
    
	return 0;
}
