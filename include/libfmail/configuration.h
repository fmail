/*
libfmail: Simple Configuration Class

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

class Configuration{
private:
	std::map<std::string, std::string> conf_map;
public:
	Configuration(const char *filename = 0);
	~Configuration();

	int Load(const char *filename);
	int Save(const char *filename);

	std::string getString(const char *key, const char *default_value);
	const char *getCString(const char *key, const char *default_value);
	int getInt(const char *key, int default_value);
	float getFloat(const char *key, float default_value);

	void setString(const char *key, const char *value);
	void setInt(const char *key, int value);
	void setFloat(const char *key, float value);
};
