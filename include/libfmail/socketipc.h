/*
libfmail: Socket Based IPC mechanism

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

class SocketIPC : public IPC{
	const char *host;
	int port;
	Socket *sock;
	Socket *auxsock;
	queue<IPCMessage*> msgQueue;
	
	int ParseLoose();
	int FetchMessage2();

public:
	SocketIPC(Socket *s);
	SocketIPC(char *uri);
	int RequestIPC();
	int ListenIPC();
	int CloseIPC();
	
	int PeekMessage();
	
	int SendMessage(IPCMessage *msg);
	IPCMessage *ReciveMessage();
	int PushMessage(IPCMessage *msg);
	IPCMessage *PopMessage();
	
	int RawRead(char *buff, int size);
	int RawWrite(char *buff, int size);
};

