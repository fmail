/*
libfmail: Socket abstraction

Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

enum {SOCKET_INET = 1, SOCKET_UNIX = 2};
enum {SOCKET_POLL_READ = 1, SOCKET_POLL_WRITE = 2, SOCKET_POLL_ERROR = 4};
enum {SOCKET_NONBLOCK = 1};

class Socket{
public:
	static Socket *CreateSocket(int socktype, int options);
	virtual ~Socket(){}
	virtual void setPort(int port) = 0;
	virtual int getPort() = 0;
	virtual void setAddress(const char *addr) = 0;
	virtual char *getAddress() = 0;

	virtual int Connect() = 0;
	virtual int Close() = 0;
	virtual int Bind() = 0;
	virtual int Listen(int queue) = 0;
	virtual Socket *Accept() = 0;

	virtual int Write(const char *buffer, int len) = 0;
	virtual int Read(char *buffer, int len) = 0;
	virtual int Poll(int ms, int mode) = 0;
};

