/*
   libfmail: IPC mechanism

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <pcrecpp.h>

#include <queue>

#include <libfmail/socket.h>
#include <libfmail/ipcmsg.h>
#include <libfmail/ipc.h>
#include <libfmail/socketipc.h>


IPC * IPC::CreateIPC(const char * ipc_uri){
	const char * str, * uri;
	IPC * ret = 0;
	string proto, opts;

	pcrecpp::RE re("([\\w\\a]+)\\:\\/\\/([\\w\\a.\\/\\:]*)");

	if (!re.PartialMatch(ipc_uri, &proto, &opts)){
		printf("Invalid URI!\n");
		return ret;
	}

	str = proto.c_str();

	if (!strcmp("socket", str)){
		uri = opts.c_str();
		ret = new SocketIPC((char *)uri);
	}

	return ret;
}
