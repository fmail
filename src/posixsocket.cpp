/*
   libfmail: Socket abstraction, Posix Implementation

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <fcntl.h>

#include <libfmail/socket.h>

/* TODO: Add proper Unix socket support
 * TODO: Add better Poll implementation
 * TODO: Add proper NONBLOCK implementation
 * */
class PosixSocket : public Socket {
private:
	int sock;
	int sock_port;
	struct sockaddr_in * sock_addr;
	struct pollfd sock_poll;
public:
	PosixSocket(int socktype, int options){
		int family;
		int flags;

		if (socktype == SOCKET_INET){
			family = AF_INET;
		} else if (socktype == SOCKET_UNIX){
			family = AF_UNIX;
		}

		sock = socket(family, SOCK_STREAM, 0);
		sock_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
		memset(sock_addr, 0, sizeof(*sock_addr));
		sock_addr->sin_family = family;
		sock_poll.fd = sock;

		if (options | SOCKET_NONBLOCK){
			flags = fcntl(sock, F_GETFL, 0);
			fcntl(sock, F_SETFL, flags|O_NONBLOCK);
		}
	}

	PosixSocket(int s, struct sockaddr_in * addr){
		sock = s;
		sock_addr = addr;
	}

	~PosixSocket(){
		free(sock_addr);
	}

	void setPort(int port){
		sock_port = port;
		sock_addr->sin_port = htons(port);
	}

	int getPort(){
		return sock_port;
	}

	void setAddress(const char * addr){
		if (addr == 0){
			sock_addr->sin_addr.s_addr = htonl(INADDR_ANY);
		} else{
			inet_pton(AF_INET, addr, &(sock_addr->sin_addr));
		}
	}

	char *getAddress(){
		char * addr;
		addr = (char *)malloc(sizeof(char) * 17);
		if (!addr)
			return 0;
		inet_ntop(AF_INET, &(sock_addr->sin_addr), addr, 17);
		return addr;
	}

	int Connect(){
		size_t slen;

		slen = sizeof(struct sockaddr_in);
		return connect(sock, (struct sockaddr *)(sock_addr), slen);
	}

	int Bind(){
		int slen;

		slen = sizeof(struct sockaddr_in);
		return bind(sock, (struct sockaddr *)sock_addr, slen);
	}

	int Listen(int queue){
		return listen(sock, queue);
	}

	int Write(const char * buffer, int len){
		return write(sock, buffer, len);
	}

	int Read(char * buffer, int len){
		return read(sock, buffer, len);
	}

	Socket *Accept(){
		int client_socket;
		struct sockaddr_in * client;
		socklen_t clen;
		Socket * s;

		client = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
		if (!client)
			return 0;
		clen = sizeof(struct sockaddr_in);

		client_socket = accept(sock, (struct sockaddr *)client, &clen);

		if (client_socket == -1)
			return 0;
		s = new PosixSocket(client_socket, client);
		return s;
	}

	int Close(){
		return close(sock);
	}

	int Poll(int ms, int mode){
		int ret;

		ret = 0;
		sock_poll.fd = sock;
		sock_poll.events = 0;

		if (mode & SOCKET_POLL_READ)
			sock_poll.events |= POLLIN;
		if (mode & SOCKET_POLL_WRITE)
			sock_poll.events |= POLLOUT;
		if (mode & SOCKET_POLL_ERROR)
			sock_poll.events |= POLLERR;

		ret = poll(&sock_poll, 1, ms);
		if (ret == -1) {
			ret |= SOCKET_POLL_ERROR;
			return ret;
		}

		if (sock_poll.revents & POLLIN)
			ret |= SOCKET_POLL_READ;
		if (sock_poll.revents & POLLOUT)
			ret |= SOCKET_POLL_WRITE;
		if (sock_poll.revents & POLLERR)
			ret |= SOCKET_POLL_ERROR;

		return ret;
	}
};

Socket * Socket::CreateSocket(int socktype, int options){
	return new PosixSocket(socktype, options);
}

