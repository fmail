/*
   libfmail: Semaphore class

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <semaphore.h>
#include <libfmail/semaphore.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

struct _ossem {
	sem_t * sem;
};

/* TODO: Add support for shared semaphores */
Semaphore::Semaphore(int value, int shared, char * key){
	this->os_sem = (OSSem *)malloc(sizeof(OSSem));
	this->sem_shared = shared;

	this->sem_name = NULL;
	if (key != NULL){
		this->sem_name = (char *)malloc(strlen(key)+1);
		strcpy(this->sem_name, key);
	}

	if (shared){
		this->os_sem->sem = sem_open(key, O_CREAT);
	} else{
		this->os_sem->sem = (sem_t *)malloc(sizeof(sem_t));
		sem_init(this->os_sem->sem, 0, value);
	}
}
Semaphore::~Semaphore(){
	if (sem_shared){
		sem_close(os_sem->sem);
		sem_unlink(sem_name);
		free(sem_name);
	} else{
		sem_destroy(os_sem->sem);
		free(os_sem->sem);
	}
	free(os_sem);
}

int Semaphore::TryWait(){
	return sem_trywait(os_sem->sem);
}

int Semaphore::Wait(){
	return sem_wait(os_sem->sem);
}

int Semaphore::Post(){
	return sem_post(os_sem->sem);
}

int Semaphore::getValue(int * val){
	return sem_getvalue(os_sem->sem, val);
}
