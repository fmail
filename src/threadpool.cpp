/*
   libfmail: Threaded Load handler with Thread Pool

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <libfmail.h>

/* Worker Thread, has a sempahore for signaling */
class ThreadLoadHandler : public Thread, public Semaphore {
private:
	Socket * s;
	ProtocolHandler * ph;
public:
	int onRun;
	ThreadLoadHandler(ProtocolHandler * pht) : Thread(), Semaphore(){
		s = NULL;
		ph = pht;
		onRun = 1;
	}

	/* Set client socket */
	void setSocket(Socket * sock){
		s = sock;
	}

	/* Deprecated */
	int isReady(){
		return 0;
	}

	/* Thread code */
	int Run(){
		while (onRun){
			/* Wait for a job, decrement semaphore */
			Wait();

			if (s){
				/* Run the handler */
				ph->Handle(s);
			}

			s = NULL;

			/* Decrement the semaphore */
			Wait();
		}
		return 0;
	}
};

ThreadLoad::ThreadLoad(ProtocolHandler * ph, int tpoolSize){
	int i;
	poolSize = tpoolSize;
	ThreadLoadHandler * * tpool;

	/* Allocate an array for the worker threads */
	pool = (void * *)malloc(sizeof(ThreadLoadHandler*) * poolSize);
	tpool = (ThreadLoadHandler * *)pool;

	/* Create and start worker threads, all should just wait for job */
	for (i = 0; i < poolSize; i++){
		tpool[i] = new ThreadLoadHandler(ph);
		tpool[i]->Start();
	}
}

ThreadLoad::~ThreadLoad(){
	int unclean, i;
	ThreadLoadHandler * * tpool;

	tpool = (ThreadLoadHandler * *)pool;

	unclean = 1;
	while(unclean){
		unclean = 0;
		for(i = 0; i < poolSize; i++){
			if (tpool[i]->onRun){
				if (tpool[i]->isReady()){
					tpool[i]->onRun = 0;
					tpool[i]->Post();
				} else{
					unclean = 1;
				}
			}
		}
	}

	for(i = 0; i < poolSize; i++){
		delete tpool[i];
	}
	free(pool);
	pool = NULL;
}

/* Boss thread handling code */
int ThreadLoad::Dispatch(Socket * sock, ProtocolHandler * ph){
	int i, dispatched;
	ThreadLoadHandler * * tpool;

	(void)ph;
	tpool = (ThreadLoadHandler * *)pool;

	dispatched = 0;
	i = 0;

	while (dispatched == 0){
		/* Check semaphore value, if 0 the thread is waiting for work */
		//tpool[i]->getValue(&v);
		//if (v < 1){
		if (tpool[i]->TryWait() == -1){
			/* Handle the socket to the thread (work) */
			//printf("Dispatching to thread %i\n", i);
			tpool[i]->setSocket(sock);

			/* Increment twice the semaphore, this to prevent
			 * handling work to a thread already doing something but
			 * with semaphore value of 0 */
			tpool[i]->Post();
			tpool[i]->Post();
			tpool[i]->Post();
			dispatched = 1;
		} else{
			tpool[i]->Post();
		}
		i++;
		/* Wrap i around pool size */
		if (i == poolSize)
			i = 0;
	}
	return 0;
}

