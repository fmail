/*
   libfmail: Thread class

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include <stdio.h>
#include <malloc.h>
#include <pthread.h>

#include <libfmail/thread.h>

struct _osthread {
	pthread_t thread;
};

extern "C" {
	typedef void * (*func_t)(void *);
	void Thread_pthread_handler(void * th){
		Thread * t;

		t = (Thread *)th;
		t->Run();
	}

}

Thread::Thread(){
	os_thread = (OSThread *)malloc(sizeof(OSThread));
}

Thread::~Thread(){
	free(os_thread);
}

int Thread::Start(){
	return pthread_create(&(os_thread->thread), NULL, (func_t)Thread_pthread_handler, this);
}

int Thread::Stop(){
	return pthread_cancel(os_thread->thread);
}

int Thread::Join(){
	return pthread_join(os_thread->thread, NULL);
}

int Thread::isAlive(){
	return 0;
}



