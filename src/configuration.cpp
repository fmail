/*
   libfmail: Generic Search Tree implementation

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <string>
#include <map>
#include <fstream>
#include <libfmail/configuration.h>

/* TODO: Do checking of file inputs */
Configuration::Configuration(const char * filename){
	if (filename)
		this->Load(filename);
}

Configuration::~Configuration(){
}

int Configuration::Load(const char * filename){
	std::fstream conf;
	std::string buffer, vname, eqop, vvalue;

	/* Open Stream to file */
	conf.open(filename);

	if (conf.is_open()){
		while (!conf.eof()){
			conf >> vname;
			conf >> eqop;
			conf >> std::ws >> vvalue;
			if (eqop == "=")
				conf_map[vname] = vvalue;
		}
		return 0;
	}

	return -1;
}

/* TODO: Implement saving */
int Configuration::Save(const char * filename){
	if (filename)
		return 0;
	return 1;
}

std::string Configuration::getString(const char * key, const char * default_value){
	std::map<std::string, std::string>::iterator it;
	std::string ret;

	if (conf_map.find(key) == conf_map.end()){
		return std::string(default_value);
	}

	return conf_map[key];
}

const char * Configuration::getCString(const char * key, const char * default_value){
	if (conf_map.find(key) == conf_map.end()){
		return default_value;
	}

	return conf_map[key].c_str();
}

int Configuration::getInt(const char * key, int default_value){
	if (conf_map.find(key) == conf_map.end()){
		return default_value;
	}

	return atoi(conf_map[key].c_str());
}

float Configuration::getFloat(const char * key, float default_value){
	if (conf_map.find(key) == conf_map.end()){
		return default_value;
	}

	return atof(conf_map[key].c_str());
}

/* TODO: Fill setString */
void Configuration::setString(const char * key, const char * value){
	conf_map[key] = std::string(value);
}

/* TODO: Fill setInt */
void Configuration::setInt(const char * key, int value){
	char buffer[32];

	sprintf(buffer, "%i", value);
	conf_map[key] = std::string(buffer);
}

/* TODO: Fill setFloat */
void Configuration::setFloat(const char * key, float value){
	char buffer[32];

	sprintf(buffer, "%f", value);
	conf_map[key] = std::string(buffer);
}


