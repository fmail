/*
   libfmail: IPC Message

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <queue>
#include <libfmail/ipcmsg.h>

char *strcopy(const char * s){
	char * ret;
	size_t l;

	l = strlen(s);
	ret = (char *)malloc(sizeof(char) * (l + 1));
	if (!ret)
		return 0;

	strcpy(ret, s);

	return ret;
}

/* TODO: Manage strings, reduce memory leaks */
IPCMessage::IPCMessage(const char * msgname){
	msghead = strcopy(msgname);
	payload = strlen(msghead);
}

IPCMessage::~IPCMessage(){
	free(msghead);
}

const char * IPCMessage::GetMessageName(){
	return msghead;
}

size_t IPCMessage::ParamCount(){
	return params.size();
}

size_t IPCMessage::GetPayload(){
	return payload;
}

void IPCMessage::PushParam(const char * param){
	payload += strlen(param);
	params.push(strcopy(param));
}

char * IPCMessage::PopParam(){
	char * r;

	if (params.empty()) {
		return 0;
	}
	r = params.front();
	params.pop();
	payload -= strlen(r);

	return r;
}
