/*
libfmail: Thread Worker Pattern Implementation

Copyright (C) 2008  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <libfmail.h>

Worker::Worker() : Thread(), Lock(){
    cJob = NULL;
    mtx = new Lock();
    mtx->Acquire();
}

Worker::~Worker(){
    delete mtx;
}

int Worker::setJob(Job *job, bool autodelete){
    cJob = job;
    autodel = autodelete;
    
    return 0;
}
int Worker::wakeup(){
    return mtx->Release();
}

int Worker::Run(){
    onRun = 1;
    while (onRun){
        mtx->Acquire();
        if (cJob){
            cJob->Run();
        }
        if (autodel)
            delete cJob;
        cJob = NULL;
        Release();
    }
    return 0;
}

Boss::Boss(int n){
    int i;
    workers = (Worker**)malloc(sizeof(Worker*) * n);
    for (i = 0; i < n; i++){
        workers[i] = new Worker();
        workers[i]->Start();
    }
    nworkers = n;
}

Boss::~Boss(){
    int i;
    for (i = 0; i < nworkers; i++){
        delete workers[i];
    }
    free(workers);
    workers = NULL;
}

int Boss::Dispatch(Job *jb, bool autodelete){
    int i = 0;
    int onRun = 1;
        
    while(onRun){
        if (workers[i]->TryAcquire() == 0){
            workers[i]->setJob(jb, autodelete);
            workers[i]->wakeup();
            onRun = 0;
        }
        i++;
        if (i == nworkers)
            i = 0;
    }
    return 1;
}


