#!/bin/sh
for arg in `find -name *.cpp`; do
	ret=`grep -n TODO $arg`
	if [ -n "$ret" ]
	then
		echo "File $arg"
		echo "$ret"
		echo ""
	else
		continue
	fi
done
