/*
   POP3 Protocol Handler

   Copyright (C) 2007  Carlos Daniel Ruvalcaba Valenzuela <clsdaniel@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <libfmail.h>
#include <pcrecpp.h>

const char * demomsg = "From: Test John <testjohn@localhost>\n"
                       "To: test@localhost\n"
                       "Content-Type: text/plain\n"
                       "Date: Wed, 06 Jun 2007 15:21:21 -0700\n"
                       "Message-Id: <1181168481.15047.2.camel@localhost>\n"
                       "Mime-Version: 1.0\n"
                       "X-Mailer: Evolution 2.10.1 \n"
                       "Content-Transfer-Encoding: 7bit\n"
                       "\n"
                       "Test Mail!!";

const int POP3_PORT = 12001; //25;
const char CRLF[2] = {0x0D, 0x0A};
const char *welcome = "+OK FancyMail v0.1";

/* Pop3 Commands */
const int CMD_USER = 1;
const int CMD_PASS = 2;
const int CMD_STAT = 3;
const int CMD_LIST = 4;
const int CMD_RETR = 5;
const int CMD_DELE = 6;
const int CMD_NOOP = 7;
const int CMD_QUIT = 8;
const int CMD_RSET = 9;
const int CMD_TOP = 10;
const int CMD_CAPA = 10;

const char KEY_END_DATA[6] = {0x0D, 0x0A, '.', 0x0D, 0x0A, 0x00};

/* Server state */
enum {STATE_AUTH = 1, STATE_AUTH2 = 2, STATE_TRANSACTION = 3,
      STATE_UPDATE = 4};

int knhash(char v){
	if ((v >= 'A') && (v <= 'Z'))
		return v - 'A';

	if (v == '_')
		return 26;

	if ((v >= 'a') && (v <= 'z'))
		return v - 'a';

	if (v == ' ')
		return -1;

	if (v == 0x0D)
		return -1;

	return 255;
}

class POP3Handler : public Job {
	SearchTree<int, 27, &knhash> cmdtree;
    Socket * s;
public:
	POP3Handler(Socket *sock) : Job() {
		/* Insert POP3 commands to search tree, 4 letters only for optimization */
		cmdtree.Insert("USER", 1);
		cmdtree.Insert("PASS", 2);
		cmdtree.Insert("STAT", 3);
		cmdtree.Insert("LIST", 4);
		cmdtree.Insert("RETR", 5);
		cmdtree.Insert("DELE", 6);
		cmdtree.Insert("NOOP", 7);
		cmdtree.Insert("QUIT", 8);
		cmdtree.Insert("RSET", 9);
		cmdtree.Insert("TOP", 10);
		cmdtree.Insert("CAPA", 11);
		printf("SearchTree Memory Usage: %i bytes\n", cmdtree.getTreeSize());
        this->s = sock;
	}
    ~POP3Handler(){
        delete s;
    }
	int Run(){
		char buffer[255];
		//char userbuff[255];
		//char outbuff[255];
		int r, i, j;
		int onrun, cmd, state;
		//IPC * ipc, * ipcmb;
		//IPCMessage * msg;
		string user, pass;
		pcrecpp::RE reg("[\\w\\a]*\\s([\\w\\a]*)");

		//ipc = IPC::CreateIPC("socket://127.0.0.1:14000");
		//ipcmb = IPC::CreateIPC("socket://127.0.0.1:14001");

		/* Send greeting to client */
		s->Write(welcome, strlen(welcome));
		s->Write(CRLF, 2);

		state = STATE_AUTH;

		onrun = 1;

		while (onrun){
			/* Wait for client input */
			memset(buffer, 0, 255);
			r = s->Read(buffer, 255);

			/* Lookup the command in out command search tree */
			try{
				cmd = cmdtree.Lookup(buffer);
			} catch (SearchNotFound){
				cmd = -1;
			}

			if (cmd == CMD_QUIT){
				printf("Got QUIT\n");

				s->Write( "+OK", 3);
				s->Write( CRLF, 2);

				/* Change state to UPDATE, will quit after that */
				state = STATE_UPDATE;
			}

			switch (state){
			case STATE_AUTH:
				if (cmd == CMD_USER){
					/* Parse username from command */
					reg.PartialMatch(buffer, &user);
					printf("Got USERNAME: %s\n", user.c_str());

					s->Write( "+OK", 3);
					s->Write( CRLF, 2);

					/* Change our state to expect password */
					state = STATE_AUTH2;
				} else{
					printf("INVALID [AUTH]: %s\n", buffer);
					s->Write( "-ERR", 4);
					s->Write( CRLF, 2);
				}
				break;
			case STATE_AUTH2:
				if (cmd == CMD_PASS){
					/* Extract password from command */
					reg.PartialMatch(buffer, &pass);

					printf("Got Password: %s\n", pass.c_str());

					if (1){
						s->Write( "+OK", 3);
						s->Write( CRLF, 2);

						/* If we got a positive response move to next state */
						state = STATE_TRANSACTION;
					} else{
						s->Write( "-ERR", 4);
						s->Write(CRLF, 2);

						/* Authentication failed, change to initial state */
						state = STATE_AUTH;
					}

				} else{
					printf("INVALID [AUTH2]: %s\n", buffer);
					s->Write( "-ERR", 4);
					s->Write(CRLF, 2);
					state = STATE_AUTH;
				}
				break;
			case STATE_TRANSACTION:
				switch (cmd){
				case CMD_STAT:
					/* Return server status (OK) */
					printf("Got STAT Command\n");
					s->Write( "+OK 3 400", 9);
					s->Write( CRLF, 2);
					break;
				case CMD_LIST:
					/* TODO: Consult the mailbox server and send the real list */
					printf("Got LIST Command: %s\n", buffer);
					s->Write( "+OK", 3);
					s->Write( CRLF, 2);

					s->Write( "1 150", 5);
					s->Write( CRLF, 2);

					s->Write( "2 150", 5);
					s->Write( CRLF, 2);

					s->Write( "3 100", 5);
					s->Write( CRLF, 2);
					s->Write( ".", 1);
					s->Write( CRLF, 2);
					break;
				case CMD_RETR:
					/* TODO: Retrieve the real message from mailbox */
					printf("Got RETR Command: %s\n", buffer);
					s->Write( "+OK 150 octets", 14);
					s->Write( CRLF, 2);

					s->Write( demomsg, strlen(demomsg));

					s->Write( CRLF, 2);
					s->Write( ".", 1);
					s->Write( CRLF, 2);
					break;
				case CMD_DELE:
					/* TODO: Delete the real message from mailbox */
					printf("Got DELE Command: %s\n", buffer);
					s->Write( "+OK", 3);
					s->Write( CRLF, 2);
					break;
				case CMD_NOOP:
					printf("Got NOOP Command\n");
					s->Write( "+OK", 3);
					s->Write( CRLF, 2);
					break;
				case CMD_RSET:
					printf("Got RSET Command\n");
					s->Write( "+OK", 3);
					s->Write( CRLF, 2);
				case CMD_TOP:
					/* TODO: Return the real top messages from mailbox */
					printf("Got TOP Command: %s\n", buffer);
					for (i = strlen(buffer); i > 0; i--){
						if (buffer[i] == ' ')
							break;
					}
					if (i >= 0){
						i = atoi((char *)(buffer + i));
					} else{
						i = 0;
					}
					s->Write( "+OK", 3);
					s->Write( CRLF, 2);

					j =0;
					for (r = 0; r < i; r++){
						sscanf((char *)(demomsg+j), "%s", buffer);
						j = j + strlen(buffer);
						s->Write( buffer, strlen(buffer));
						s->Write( CRLF, 2);
					}
					s->Write( ".", 1);
					s->Write( CRLF, 2);
					break;
				default:
					printf("INVALID [TRANS]: %s\n", buffer);
					s->Write( "-ERR", 4);
					s->Write( CRLF, 2);
				}
				break;
			case STATE_UPDATE:
				onrun = 0;
				break;
			}
		}
        
		s->Close();
		return 0;
	}
};

class SimpleLoad : public LoadHandler {
public:
	int Dispatch(Socket * sock, ProtocolHandler * ph){
		if (ph)
			return ph->Handle(sock);
		return 0;
	}
};

int main(){
	Socket * s, * client;
    Boss *boss;
	int ret;

	/* Create our POP3 Handler and Threaded Load Handler */
	//pop3 = new POP3Handler();
	//tlh = new ThreadLoad(pop3);
    boss = new Boss(10);

	/* Create and bind our socket to POP3 port */
	s = Socket::CreateSocket(SOCKET_INET, 0);
	s->setPort(POP3_PORT);
	s->setAddress("127.0.0.1");
	s->Bind();
	s->Listen(15);

	while(1){
		/* Poll for incoming connections */
		ret = s->Poll(1000000, SOCKET_POLL_READ | SOCKET_POLL_ERROR);

		if (ret & SOCKET_POLL_READ){
			/* Accept client and dispatch */
			client = s->Accept();
            boss->Dispatch(new POP3Handler(client));
		} else if(ret & SOCKET_POLL_ERROR){ //error
			return -1;
		} else{ //timeout
			break;
		}
	}
	delete boss;
	return 0;
}

